"""This module implements a set of functions related to saving data.

Functions
---------
create_url(url)
    Save an URL in database.
save_in_file(url)
    Write an URL in text file.
"""
from typing import Set
import sqlite3

from src.settings import Settings


def create_url(url: str) -> None:
    """Saves an URL in database.
    
    Parameters
    ----------
    url : str
        The URL to save.
    """
    conn = sqlite3.connect(Settings().db_name)
    c = conn.cursor()
    c.execute("""INSERT OR REPLACE INTO 
              pages (url) VALUES (?)""", (url,))
    conn.commit()
    conn.close()


def save_in_file(url: str) -> None:
    """Writes an URL in predefined textfile.

    Parameters
    ----------
    url : str
        The URL to save.
    """
    with open(Settings().export_file_name, 'a') as file:
        file.write(url + "\n")
