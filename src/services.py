"""This module implements a set of functions to use in the app.

Functions
---------
is_allowed(url, user_agent)
    Checks whether the website allows selected URL to be scraped
get_delay(url, user_agent)
    Returns the delay to follow if it exists, a predetermined one otherwise.
read_urls_in_page(url, max_links)
    Reads all urls in a web page.
read_urls_in_sitemap(sitemap_url)
    Read all urls shown in a sitemap.
"""
from typing import Set
import requests
import xml.etree.ElementTree as ET

from bs4 import BeautifulSoup
from urllib.parse import urlparse
from urllib.request import urljoin
from urllib.robotparser import RobotFileParser

from src.exceptions import (RequestException,
                            SitemapCrawlException,
                            PageCrawlException)
from src.settings import Settings


def is_allowed(url: str, user_agent: str = "*") -> bool:
    """Checks whether the website allows selected URL to be scraped.

    Parameters
    ----------
    url : str
        The url to check.
    user_agent : str, optional
        The user agent to check for in robots.txt, by default "*"

    Returns
    -------
    bool
        True if scraping is allowed, False otherwise.
    """
    parser = RobotFileParser(urljoin(url, "/robots.txt"))
    parser.read()
    return parser.can_fetch(user_agent, url)


def get_delay(url: str, user_agent: str = "*") -> int:
    """Get politeness delay needed in considered website.
    
    If a delay is specified in robots.txt, returns it. Otherwise, returns a
    predefined custom delay.

    Parameters
    ----------
    url : str
        The url of the website whose delay to check.
    user_agent : str, optional
        The user_agent to check for in robots.txt, by default "*"

    Returns
    -------
    int
        The politeness delay to follow on selected website.
    """
    parser = RobotFileParser(urljoin(url, "/robots.txt"))
    parser.read()
    delay = parser.crawl_delay(user_agent)
    if delay:
        return int(delay)
    else:
        return Settings().custom_delay


def _read_page_content(url: str) -> bytes:
    """Read contents of a page from url.

    Parameters
    ----------
    url : str
        The url to fetch.

    Returns
    -------
    bytes
        The content of the web page.

    Raises
    ------
    Exception
        Raised when request response isn't as expected.
    """
    response = requests.get(url)
    if response.status_code == 200:
        return response.content
    raise RequestException(f"Error while fetching at url {url}")


def read_urls_in_page(url: str, max_links: int) -> Set[str]:
    """Reads all urls in a web page.
    
    Using BeautifulSoup, goes throug all hrefs in source code, parses them to
    absolute URLs if needed, and stores them if they are in the right website,
    i.e shares a common root domain.

    Parameters
    ----------
    url : str
        The url of the page to scan.
    max_links : int
        Maximum number of URLs to fetch in page.

    Returns
    -------
    Set[str]
        The new found urls.
    """
    try:
        content = _read_page_content(url)
        soup = BeautifulSoup(content, 'lxml')
        hrefs = soup.find_all('a', href=True)

        new_urls = set()
        index = 0

        while len(new_urls) < max_links and index < len(hrefs):
            href = hrefs[index].get('href')
            final_url = urljoin(url, href)
            index += 1
            if urlparse(final_url).netloc == urlparse(url).netloc:
                new_urls.add(final_url)
        return new_urls
    except RequestException as exc:
        raise PageCrawlException from exc


def read_urls_in_sitemap(sitemap_url: str) -> Set[str]:
    """Read all urls shown in a sitemap.
    
    It works recursively, to make sure that if a sitemap points to another
    sitemap, we actually work through the whole tree to extract all urls.

    Parameters
    ----------
    sitemap_url : str
        URL of the sitemap page.

    Returns
    -------
    Set[str]
        The new URLs found in the sitemap.
    """
    try:
        content = _read_page_content(sitemap_url)
        root = ET.fromstring(content)
        new_urls = []
        for child in root:
            if 'url' in child.tag:
                    for subchild in child:
                        if 'loc' in subchild.tag:
                            new_urls.append(subchild.text)
            elif 'sitemap' in child.tag:
                for subchild in child:
                    if 'loc' in subchild.tag:
                        new_urls.extend(read_urls_in_sitemap(subchild.text))
        return set(new_urls)
    except RequestException as exc:
        raise SitemapCrawlException from exc
