"""This module implements a main class to access all settings needed in app.


Classes
-------
Settings
    Computes all necessary settings used in the app.
"""
from pydantic_settings import BaseSettings


class Settings(BaseSettings):
    """Computes all necessary settings used in the app.

    Attributes
    ----------

    """
    db_name: str = 'output/database.db'
    export_file_name: str = 'output/crawled_webpages.txt'
    custom_delay: int = 1
