"""This module imlements custom exceptions.
"""


class RequestException(Exception):
    """Raised when trouble requesting an URL."""


class SitemapCrawlException(Exception):
    """Raised when error happening during crawling a sitemap."""


class PageCrawlException(Exception):
    """Raised when error happening during crawling a page."""
