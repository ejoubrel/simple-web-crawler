"""This module implements a class for Crawling through webpages.

Classes
-------
Crawler
    The main app crawler.
"""
import time
from collections import deque

from urllib.request import urljoin

from src.exceptions import SitemapCrawlException, PageCrawlException
from src.services import read_urls_in_page, read_urls_in_sitemap, is_allowed, get_delay
from src.dao import create_url, save_in_file


class Crawler:
    """This class is the main class of the app and implements a crawler.
    
    Attributes
    ----------
    base_url : str
        The base URL of the website to crawl.
    max_pages: int
        The maximum number of pages to be crawled.
    max_links_per_page : int
        The maximum numbers of links to extract from any page.
    visited_urls : Set[str]
        All URLs fetched by the Crawler at the moment.
    queue : deque[str]
        Queue of URLs to be visited by the crawler.
    delay: int
        Number of seconds to wait between two requests.
    
    Methods
    -------
    initialize_queue(self)
        Initialize queue with sitemap URLs if possible.
    crawl(self)
        Perform the crawling operation.
    save_urls(self)
        Save all visited URLs in database and text file.
    """
    def __init__(self, base_url, max_pages, max_links_per_page):
        self.base_url = base_url
        self.max_pages = max_pages
        self.max_links_per_page = max_links_per_page
        self.visited_urls = set()
        self.queue = deque([])
        self.delay = get_delay(base_url)
    
    def initialize_queue(self) -> None:
        """Initialize the queue.
        
        If URLs are found in the sitemap, adds them to queue. Otherwise, only
        adds the main page.
        """
        try:
            sitemap_url = urljoin(self.base_url, '/sitemap.xml')
            new_urls = read_urls_in_sitemap(sitemap_url)
            self.queue.extend(list(new_urls)[:self.max_links_per_page])
        except SitemapCrawlException:
            self.queue.add(self.base_url)
    
    def crawl(self) -> None:
        """Main method, crawls through all web pages.
        
        Works through URLs in queue. For each, gets all URLs in source code,
        and adds them to queue if they fit. After each requests, waits the
        requested amount of time.
        """
        while len(self.visited_urls) < self.max_pages and self.queue:
            url = self.queue.popleft()
            if url not in self.visited_urls and is_allowed(url):
                try:
                    new_urls = read_urls_in_page(url, self.max_links_per_page)
                except PageCrawlException:
                    new_urls = []
                self.visited_urls.add(url)
                for new_url in new_urls:
                    self.queue.append(new_url)
                time.sleep(self.delay)
    
    def save_urls(self) -> None:
        """Saves visited URLs in both ways.
        """
        for url in self.visited_urls:
            create_url(url)
            save_in_file(url)
