"""This module implements functions related to the database.

Function
--------
create_db()
    Creates database and table.
"""
import sqlite3

from src.settings import Settings

def create_db():
    """Creates database and table.
    """
    conn = sqlite3.connect(Settings().db_name)
    cursor = conn.cursor()
    cursor.execute("""CREATE TABLE IF NOT EXISTS 
                   pages (url TEXT PRIMARY KEY)""")
    conn.commit()
    conn.close()
