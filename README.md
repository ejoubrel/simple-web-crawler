# Simple Web Crawler

This project implements a simple web crawler in Python.  
The crawler, given a start page URL, a maximum number of pages and a maximum number of links per page:
- Tries to fetch URLs in the sitemap page if it exists, and adds them to the URLs to fetch.
- For every URL to fetch, goes the source code and retrieves all URLs from the same domain, until it reaches the maximum number allowed. It adds those URLs to the pages to visit.
- It works that way recursively until it reaches the maximum number allowed or has no more URLs to visit.
- Finally, it saves all visited URLs in both a text file and a relationnal database.


## Authors

- Elwenn Joubrel : [@ejoubrel](https://www.gitlab.com/ejoubrel)


## Installation

You will need python and pip, and after cloning this project:

```bash
  cd simple-web-scraper
  pip install -r requirements.txt
  python app.main
```
