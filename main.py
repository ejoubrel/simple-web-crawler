"""Main runner for the app.

Functions
---------
main()
    Runs the crawler.
"""
from src.crawler import Crawler
from src.db_setup import create_db


def main():
    """Runs the crawler.
    """
    create_db()
    crawler = Crawler(base_url="https://ensai.fr",
                      max_pages=50,
                      max_links_per_page=5)
    crawler.initialize_queue()
    crawler.crawl()
    crawler.save_urls()
    print(f"Nombre d'URLs récupérées : {len(crawler.visited_urls)}")


if __name__ == "__main__":
    main()
